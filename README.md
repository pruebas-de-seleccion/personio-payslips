Payslips!!! 
========================

# Welcome!
Hello people!!! 

First of all I hope that all is ok!! This is my solution of your challenge. It is written in Kotlin because for me it support very fast develop and a lot of helper to good practices. I hope that you found it well. 

# Description
Ok. I think that I can jump this part because you already know yours requirement :)

#What do you need before start
- Java 12 (I think that I dont use noting special form java 12 but ¯\_(ツ)_/¯
- Internet!!!!! because gradle need to download dependencies  
- You dont need gradle, I upload the wrapper for you :P
- Breathe and be aware that I am human too, and I usually make mistake :)

###Build the project!!!
This is a spring boot project built with gradle, as usual you can build it form console with:
```shell
./gradlew build
```

###Run the project!!!
This is a spring boot project built with gradle, as usual you can star up the server form console with:
```shell
./gradlew clean build bootRun
```

###All in one :)
Be careful, you only need build the project the first time. Remember: test passing in build phase.
```shell
./gradlew clean build bootRun
```

# What was my decisions?

- **Use Kotlin**: At first I choose java, but then I thought that I could demonstrate my experience in java given my CV. For that reason I change to kotlin, that help me a lot to develop quickly (this is important because today is saturday and I want to enjoy my free time). **I didn't make any annotation about kotlin language. If you need some help in order to understand better my code about syntax or properties of the language please let me know.** 
- **Use DDD**: Ok, for this challenge DDD may be to much. DDD architecture add a lot of  boilerplate to split business logic and in small project like this could be annoying. But I think that the final result is good and that DDD make the code more reusable and maintainable.
- **Use TDD**: You can see it in the commit history. I broke some commit rules because I was in hurry but you can prove that almost all code is pushing for some test.
- **Not overreach**: I decide don't overreach. I don't create anything extra as documentation, extra validation or whatever. If you need any more, only tell me. 
- **Not store payslips**: I prefer no store payslips and recalculate tax on fly.
- Create sampler (ObjectMother pattern): I love this pattern!!!! To no extend myself I only use a simple Sampler in order to generate domain object. I would have liked to create a builder for the http-request as well.

## How is it tested?
At the moment this project has the three more common type of test: unitary, integration and acceptance.
- **Acceptance test**:
	- I create some test stimulating api from outside. I need create a moked external server in order to no depends of three parties
- **Integration test**: There are not integration tests itself because almost infrastructure stuff have memory implementation. Only http client to connect with external data server.
- **Unitary test**: the rest test.

# What could be improved?
- Everything by default :D
- Input validation and descriptive response in get endpoint
- Create Test to ensure double format in json serialization

## Mandatory Gif
Because a mandatory gif always is funny and improve PR readability. 

![mandatory gif](https://media.giphy.com/media/Mliueouehmpag/giphy.gif)

**And that is all. I wish you the best.**

