package com.jorgejbarra.payslips

import com.jorgejbarra.payslips.infrastructure.configuration.ApiControllerAdvice
import com.jorgejbarra.payslips.infrastructure.configuration.ApiJacksonConfiguration
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders

object MockMvcBuilder {
  fun mockMvcOf(subjectUnderTest: Any): MockMvc = MockMvcBuilders.standaloneSetup(subjectUnderTest)
      .setMessageConverters(jacksonConverter())
      .setControllerAdvice(ApiControllerAdvice())
      .build()

  private fun jacksonConverter(): MappingJackson2HttpMessageConverter =
      MappingJackson2HttpMessageConverter(ApiJacksonConfiguration().objectMapper())
}

