package com.jorgejbarra.payslips.infrastructure.repository

import com.jorgejbarra.payslips.domain.PayslipsCriteria
import com.jorgejbarra.payslips.domain.Sampler.aRate
import com.jorgejbarra.payslips.domain.TaxRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class InMemoryTaxRepositoryShould {

  private val taxRepository: TaxRepository = InMemoryTaxRepository()

  @Test
  internal fun `return null when no tax store`() {
    val payslipsCriteria = PayslipsCriteria(2003, 8)

    assertThat(taxRepository.get(payslipsCriteria)).isNull()
  }

  @Test
  internal fun `store new tax`() {
    val updatedRate = aRate()
    val payslipsCriteria = PayslipsCriteria(2003, 8)

    taxRepository.update(payslipsCriteria, updatedRate)

    assertThat(taxRepository.get(payslipsCriteria)).isEqualTo(updatedRate)
  }

  @Test
  internal fun `update stored tax`() {
    val storedRate = aRate()
    val updatedRate = aRate()
    val payslipsCriteria = PayslipsCriteria(2012, 2)

    taxRepository.update(payslipsCriteria, storedRate)
    taxRepository.update(payslipsCriteria, updatedRate)

    assertThat(taxRepository.get(payslipsCriteria)).isEqualTo(updatedRate)
  }
}
