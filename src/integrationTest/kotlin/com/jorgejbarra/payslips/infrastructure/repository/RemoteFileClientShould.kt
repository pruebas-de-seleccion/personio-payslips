package com.jorgejbarra.payslips.infrastructure.repository

import com.github.tomakehurst.wiremock.client.WireMock.anyUrl
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.notFound
import com.github.tomakehurst.wiremock.client.WireMock.ok
import com.github.tomakehurst.wiremock.client.WireMock.stubFor
import com.jorgejbarra.payslips.infrastructure.EmptyContextIntegrationTest
import com.jorgejbarra.payslips.infrastructure.configuration.RemoteFileClientConfiguration
import com.jorgejbarra.payslips.infrastructure.repository.YellowCarsServerResponse.NotFound
import com.jorgejbarra.payslips.infrastructure.repository.YellowCarsServerResponse.PayslipsFile
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.context.annotation.Import

@AutoConfigureWireMock
@Import(RemoteFileClientConfiguration::class)
internal class RemoteFileClientShould : EmptyContextIntegrationTest() {
  @Autowired
  private lateinit var remoteFileClient: RemoteFileClient

  @Test
  internal fun `return not found when status is not found`() {
    //given
    stubFor(get(anyUrl()).willReturn(notFound()))

    //when
    val payslipsFile = remoteFileClient.getPayslipsFile("unknown-file")

    //then
    assertThat(payslipsFile).isEqualTo(NotFound)
  }

  @Test
  internal fun `return body lines as list when file is found`() {
    //given
    stubFor(get(anyUrl()).willReturn(
        ok("""000000
              111111
              222222""".trimMargin())
    ))

    //when
    val payslipsFile = remoteFileClient.getPayslipsFile("payslips.201612.txt")

    //then
    assertThat(payslipsFile).isEqualTo(PayslipsFile(listOf("000000", "111111", "222222")))
  }
}
