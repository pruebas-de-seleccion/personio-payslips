package com.jorgejbarra.payslips.infrastructure.api

import com.jorgejbarra.payslips.MockMvcBuilder.mockMvcOf
import com.jorgejbarra.payslips.application.GetPayslips
import com.nhaarman.mockito_kotlin.mock
import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import io.restassured.module.mockmvc.RestAssuredMockMvc.mockMvc
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

internal class GetPayslipsControllerShould {
  private val getPayslipsMock = mock<GetPayslips>()
  private lateinit var mockMvc: MockMvc

  @BeforeEach
  fun setUp() {
    mockMvc = mockMvcOf(GetPayslipsController(getPayslipsMock))
        .apply { mockMvc(this) }
  }

  @Test
  internal fun `return ok when get payslips`() {
    given()
        .contentType(APPLICATION_JSON_VALUE)
        .`when`()
        .get("/api/payslips?year=2012&month=JANUARY")
        .then()
        .assertThat(status().isOk)
  }

  @Test
  internal fun `return bad request when try to get payslips without year`() {
    given()
        .contentType(APPLICATION_JSON_VALUE)
        .`when`()
        .get("/api/payslips?month=JANUARY")
        .then()
        .assertThat(status().isBadRequest)
  }

  @Test
  internal fun `return bad request when try to get payslips without month`() {
    given()
        .contentType(APPLICATION_JSON_VALUE)
        .`when`()
        .get("/api/payslips?year=2016")
        .then()
        .assertThat(status().isBadRequest)
  }

  @Test
  internal fun `return bad request when year is invalid`() {
    given()
        .contentType(APPLICATION_JSON_VALUE)
        .`when`()
        .get("/api/payslips?year=-2&month=JANUARY")
        .then()
        .assertThat(status().isBadRequest)
  }
}
