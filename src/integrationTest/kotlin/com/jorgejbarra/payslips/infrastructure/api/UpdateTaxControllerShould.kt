package com.jorgejbarra.payslips.infrastructure.api

import com.jorgejbarra.payslips.MockMvcBuilder.mockMvcOf
import com.jorgejbarra.payslips.application.UpdateTax
import com.nhaarman.mockito_kotlin.mock
import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import io.restassured.module.mockmvc.RestAssuredMockMvc.mockMvc
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

internal class UpdateTaxControllerShould {
  private val updateTaxMock = mock<UpdateTax>()
  private lateinit var mockMvc: MockMvc

  @BeforeEach
  fun setUp() {
    mockMvc = mockMvcOf(UpdateTaxController(updateTaxMock))
        .apply { mockMvc(this) }
  }

  @Test
  internal fun `return ok when update tax`() {
    given()
        .contentType(APPLICATION_JSON_VALUE)
        .body("""{"taxRate":12.00}""")
        .`when`()
        .put("/api/tax?year=2012&month=JANUARY")
        .then()
        .assertThat(status().isOk)
  }

  @Test
  internal fun `return bad request when try without year`() {
    given()
        .contentType(APPLICATION_JSON_VALUE)
        .`when`()
        .put("/api/tax?month=JANUARY")
        .then()
        .assertThat(status().isBadRequest)
  }

  @Test
  internal fun `return bad request when try without month`() {
    given()
        .contentType(APPLICATION_JSON_VALUE)
        .`when`()
        .put("/api/tax?year=2016")
        .then()
        .assertThat(status().isBadRequest)
  }

  @Test
  internal fun `return bad request when year is invalid`() {
    given()
        .contentType(APPLICATION_JSON_VALUE)
        .`when`()
        .put("/api/tax?year=-2&month=JANUARY")
        .then()
        .assertThat(status().isBadRequest)
  }
}
