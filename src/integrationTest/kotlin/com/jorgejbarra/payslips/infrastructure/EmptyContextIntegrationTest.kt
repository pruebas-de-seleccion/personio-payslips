package com.jorgejbarra.payslips.infrastructure

import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension

@ActiveProfiles("test")
@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [BaseApplicationForTest::class])
abstract class EmptyContextIntegrationTest

@Configuration
@EnableAutoConfiguration
class BaseApplicationForTest
