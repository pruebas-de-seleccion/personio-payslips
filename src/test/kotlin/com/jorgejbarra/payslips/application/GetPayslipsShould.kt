package com.jorgejbarra.payslips.application

import com.jorgejbarra.payslips.domain.PayslipsCriteria
import com.jorgejbarra.payslips.domain.PayslipsRepository
import com.jorgejbarra.payslips.domain.Sampler.aPayslip
import com.jorgejbarra.payslips.domain.Sampler.aRate
import com.jorgejbarra.payslips.infrastructure.api.PayslipDTO
import com.jorgejbarra.payslips.infrastructure.repository.InMemoryTaxRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Month
import java.time.Year

internal class GetPayslipsShould {
  private val payslipsRepositoryMock = mock<PayslipsRepository>()
  private val taxRepositoryMock = InMemoryTaxRepository()
  private val service = GetPayslips(payslipsRepositoryMock, taxRepositoryMock)

  @Test
  internal fun `apply new tax when exist`() {
    //given
    val year = 2020
    val month = 2
    val aPayslip = aPayslip()
    val updateTax = aRate()

    taxRepositoryMock.update(PayslipsCriteria(year, month), updateTax)
    whenever(payslipsRepositoryMock.find(PayslipsCriteria(year, month))).thenReturn(listOf(aPayslip))

    //when
    val payslips = service.execute(Year.of(year), Month.of(month))

    //then
    assertThat(payslips).usingFieldByFieldElementComparator().containsExactly(
        PayslipDTO(
            id = aPayslip.id,
            vatIdNumber = aPayslip.vatIdNumber,
            date = aPayslip.date,
            gross = aPayslip.gross.decimalValue(),
            nationalInsuranceRate = aPayslip.nationalInsuranceRate.decimalValue(),
            nationalInsuranceDeductionsAmount = aPayslip.nationalInsuranceDeductionsAmount.decimalValue(),
            taxRate = updateTax.decimalValue(),
            taxAmount = aPayslip.gross.applyRate(updateTax).decimalValue(),
            net = aPayslip.net.plus(aPayslip.taxAmount).minus(aPayslip.gross.applyRate(updateTax)).decimalValue()
        )
    )
  }
}
