package com.jorgejbarra.payslips.infrastructure.repository

import com.jorgejbarra.payslips.domain.MonetaryAmountFactory.monetaryAmount
import com.jorgejbarra.payslips.domain.Payslip
import com.jorgejbarra.payslips.domain.RateFactory.rate
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.Test
import java.time.LocalDate

internal class PayslipDeserializerShould {
  private val payslipDeserializer = PayslipDeserializer()

  @Test
  internal fun `return unreadable when line is too short`() {
    val tooShortLine = "00000000000197084172E20181231002486000500000124301200000298320020633"

    val expectedException = catchThrowable { payslipDeserializer.read(tooShortLine) }

    assertThat(expectedException)
        .isInstanceOf(PayslipLineDeserializeException::class.java)
        .hasMessage("Line too short, it must be 69")
  }

  @Test
  internal fun `return unreadable when line is too long`() {
    val tooShortLine = "00000000000197084172E2018123100248600050000012430120000029832002063370"

    val expectedException = catchThrowable { payslipDeserializer.read(tooShortLine) }

    assertThat(expectedException)
        .isInstanceOf(PayslipLineDeserializeException::class.java)
        .hasMessage("Line too long, it must be 69")
  }

  @Test
  internal fun `return payslip`() {
    val tooShortLine = "00000000000197084172E201812310024860005000001243012000002983200206337"

    val readPayslip = payslipDeserializer.read(tooShortLine)

    assertThat(readPayslip).isEqualToComparingFieldByField(
        Payslip(
            id = "000000000001",
            vatIdNumber = "97084172E",
            date = LocalDate.of(2018, 12, 31),
            gross = monetaryAmount(248600),
            nationalInsuranceRate = rate(500),
            nationalInsuranceDeductionsAmount = monetaryAmount(12430),
            taxRate = rate(1200),
            taxAmount = monetaryAmount(29832),
            net = monetaryAmount(206337)
        )
    )
  }
}


