package com.jorgejbarra.payslips.infrastructure.repository

import com.jorgejbarra.payslips.domain.MonetaryAmountFactory.monetaryAmount
import com.jorgejbarra.payslips.domain.Payslip
import com.jorgejbarra.payslips.domain.PayslipsCriteria
import com.jorgejbarra.payslips.domain.RateFactory.rate
import com.jorgejbarra.payslips.infrastructure.repository.YellowCarsServerResponse.NotFound
import com.jorgejbarra.payslips.infrastructure.repository.YellowCarsServerResponse.PayslipsFile
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDate

internal class RemoteFilePayslipsRepositoryShould {

  private val remoteFileClientMock = mock<RemoteFileClient>()
  private val remoteFilePayslipsRepository = RemoteFilePayslipsRepository(remoteFileClientMock, PayslipDeserializer())

  @Test
  internal fun `return empty list when file not found`() {
    //given
    whenever(remoteFileClientMock.getPayslipsFile(any())).thenReturn(NotFound)

    //when
    val payslips = remoteFilePayslipsRepository.find(PayslipsCriteria(2002, 6))

    //then
    assertThat(payslips).isEmpty()
  }

  @Test
  internal fun `find expected file`() {
    //given
    whenever(remoteFileClientMock.getPayslipsFile(any())).thenReturn(NotFound)

    //when
    remoteFilePayslipsRepository.find(PayslipsCriteria(2002, 6))

    //then
    verify(remoteFileClientMock).getPayslipsFile("payslips.200206.txt")
  }

  @Test
  internal fun `return expected lines`() {
    //given
    whenever(remoteFileClientMock.getPayslipsFile(any())).thenReturn(PayslipsFile(listOf(
        "00000000052396433625F201812310022170004000000886816000003547200177360"
    )))

    //when
    val payslips = remoteFilePayslipsRepository.find(PayslipsCriteria(2002, 6))

    //then
    assertThat(payslips).containsExactly(
        Payslip(
            id = "000000000523",
            vatIdNumber = "96433625F",
            date = LocalDate.of(2018, 12, 31),
            gross = monetaryAmount(221700),
            nationalInsuranceRate = rate(400),
            nationalInsuranceDeductionsAmount = monetaryAmount(8868),
            taxRate = rate(1600),
            taxAmount = monetaryAmount(35472),
            net = monetaryAmount(177360)
        )
    )
  }
}
