package com.jorgejbarra.payslips.domain

object MonetaryAmountFactory {
  fun monetaryAmount(value: Long) = MonetaryAmount(TwoDecimalNumber(value))
}
