package com.jorgejbarra.payslips.domain

import com.jorgejbarra.payslips.domain.Sampler.aTwoDecimalNumber
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import kotlin.random.Random.Default.nextLong

internal class TwoDecimalNumberShould {

  @Test
  internal fun `double value should have 2 decimals`() {
    //given
    val twoDecimalNumber = TwoDecimalNumber(2222)

    //then
    assertThat(twoDecimalNumber.decimalValue()).isEqualTo(22.22)
  }

  @Test
  internal fun `no modify when apply multi`() {
    //given
    val expectedNumber = nextLong(1000, 1000000)
    val aNumber = TwoDecimalNumber(expectedNumber)

    //when
    aNumber.multi(aTwoDecimalNumber())

    //then
    assertThat(aNumber).isEqualTo(TwoDecimalNumber(expectedNumber))
  }

  @Test
  internal fun multiply() {
    //given
    val aNumber = nextLong(1000, 1000000)
    val otherNumber = nextLong(1000, 1000000)

    //when
    val result = TwoDecimalNumber(aNumber).multi(TwoDecimalNumber(otherNumber))

    //then
    assertThat(result).isEqualTo(TwoDecimalNumber(aNumber.times(otherNumber)))
  }

  @Test
  internal fun `no modify when apply plus`() {
    //given
    val expectedNumber = nextLong(1000, 1000000)
    val aNumber = TwoDecimalNumber(expectedNumber)

    //when
    aNumber.plus(aTwoDecimalNumber())

    //then
    assertThat(aNumber).isEqualTo(TwoDecimalNumber(expectedNumber))
  }

  @Test
  internal fun plus() {
    //given
    val aNumber = nextLong(1000, 1000000)
    val otherNumber = nextLong(1000, 1000000)

    //when
    val result = TwoDecimalNumber(aNumber).plus(TwoDecimalNumber(otherNumber))

    //then
    assertThat(result).isEqualTo(TwoDecimalNumber(aNumber.plus(otherNumber)))
  }

  @Test
  internal fun `no modify when apply minus`() {
    //given
    val expectedNumber = nextLong(1000, 1000000)
    val aNumber = TwoDecimalNumber(expectedNumber)

    //when
    aNumber.minus(aTwoDecimalNumber())

    //then
    assertThat(aNumber).isEqualTo(TwoDecimalNumber(expectedNumber))
  }

  @Test
  internal fun minus() {
    //given
    val aNumber = nextLong(1000, 1000000)
    val otherNumber = nextLong(1000, 1000000)

    //when
    val result = TwoDecimalNumber(aNumber).minus(TwoDecimalNumber(otherNumber))

    //then
    assertThat(result).isEqualTo(TwoDecimalNumber(aNumber.minus(otherNumber)))
  }

  @Test
  internal fun `no modify when apply rate`() {
    //given
    val expectedNumber = nextLong(1000, 1000000)
    val aNumber = TwoDecimalNumber(expectedNumber)

    //when
    aNumber.applyRate(aTwoDecimalNumber())

    //then
    assertThat(aNumber).isEqualTo(TwoDecimalNumber(expectedNumber))
  }

  @Test
  internal fun `apply rate`() {
    //given
    val aNumber = 20000L
    val otherNumber = 1000L

    //when
    val result = TwoDecimalNumber(aNumber).applyRate(TwoDecimalNumber(otherNumber))

    //then
    assertThat(result.decimalValue()).isEqualTo(20.00)
  }
}
