package com.jorgejbarra.payslips.domain

import java.time.LocalDate
import java.util.UUID.randomUUID
import kotlin.random.Random.Default.nextLong

object Sampler {
  fun aPayslip(): Payslip {
    val gross = aMonetaryAmount()
    val nationalInsuranceRate = aRate()
    val taxRate = aRate()
    return Payslip(
        id = aPayslipId(),
        vatIdNumber = aVatIdNumber(),
        date = LocalDate.now(),
        gross = gross,
        nationalInsuranceRate = nationalInsuranceRate,
        nationalInsuranceDeductionsAmount = gross.applyRate(nationalInsuranceRate),
        taxRate = taxRate,
        taxAmount = gross.applyRate(taxRate),
        net = gross.minus(gross.applyRate(taxRate)).minus(gross.applyRate(nationalInsuranceRate))
    )
  }

  fun aTwoDecimalNumber() = TwoDecimalNumber(nextLong())
  fun aMonetaryAmount() = MonetaryAmount(TwoDecimalNumber(nextLong(1000, 1000000)))
  fun aRate() = Rate(TwoDecimalNumber(nextLong(1, 25)))
  fun aVatIdNumber() = randomUUID().toString()
  fun aPayslipId() = randomUUID().toString()
}
