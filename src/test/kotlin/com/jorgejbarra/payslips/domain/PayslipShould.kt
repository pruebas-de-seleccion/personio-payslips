package com.jorgejbarra.payslips.domain

import com.jorgejbarra.payslips.domain.Sampler.aPayslip
import com.jorgejbarra.payslips.domain.Sampler.aRate
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class PayslipShould {

  @Test
  internal fun `update tax rate`() {
    val aPayslip = aPayslip()
    val updatedTaxRate = aRate()

    val updatePayslip = aPayslip.updateTaxRate(updatedTaxRate)

    assertThat(updatePayslip.taxRate).isEqualTo(updatedTaxRate)
  }

  @Test
  internal fun `update amount tax when tax rate is changed`() {
    val aPayslip = aPayslip()
    val updatedTaxRate = aRate()

    val updatePayslip = aPayslip.updateTaxRate(updatedTaxRate)

    assertThat(updatePayslip.taxAmount).isEqualTo(aPayslip.gross.applyRate(updatedTaxRate))
  }

  @Test
  internal fun `update net when tax rate is changed`() {
    val aPayslip = aPayslip()
    val updatedTaxRate = aRate()

    val updatePayslip = aPayslip.updateTaxRate(updatedTaxRate)

    assertThat(updatePayslip.net).isEqualTo(aPayslip.net
        .plus(aPayslip.gross.applyRate(aPayslip.taxRate))
        .minus(aPayslip.gross.applyRate(updatedTaxRate))
    )
  }

  @Test
  internal fun `dont change values when tax rate is changed by same value`() {
    val aPayslip = aPayslip()

    val updatePayslip = aPayslip.updateTaxRate(aPayslip.taxRate)

    assertThat(updatePayslip).isEqualTo(aPayslip)
  }
}
