package com.jorgejbarra.payslips.domain

import java.time.LocalDate

data class Payslip(
    val id: String,
    val vatIdNumber: String,
    val date: LocalDate,
    val gross: MonetaryAmount,
    val nationalInsuranceRate: Rate,
    val nationalInsuranceDeductionsAmount: MonetaryAmount,
    val taxRate: Rate,
    val taxAmount: MonetaryAmount,
    val net: MonetaryAmount
) {

  fun updateTaxRate(updatedTaxRate: Rate): Payslip = this.copy(
      taxRate = updatedTaxRate,
      taxAmount = gross.applyRate(updatedTaxRate),
      net = gross.minus(gross.applyRate(nationalInsuranceRate)).minus(gross.applyRate(updatedTaxRate))
  )
}
