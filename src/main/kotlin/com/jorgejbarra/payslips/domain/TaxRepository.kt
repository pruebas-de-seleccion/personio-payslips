package com.jorgejbarra.payslips.domain

interface TaxRepository {
  fun update(payslipsCriteria: PayslipsCriteria, rate: Rate)
  fun get(payslipsCriteria: PayslipsCriteria): Rate?
}
