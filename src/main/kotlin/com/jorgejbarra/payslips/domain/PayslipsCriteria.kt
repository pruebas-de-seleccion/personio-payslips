package com.jorgejbarra.payslips.domain

data class PayslipsCriteria(val year: Int, val month: Int)
