package com.jorgejbarra.payslips.domain

interface PayslipsRepository {
  fun find(criteria: PayslipsCriteria): List<Payslip>
}
