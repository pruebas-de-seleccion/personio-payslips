package com.jorgejbarra.payslips.domain

data class TwoDecimalNumber(private val value: Long) {
  fun decimalValue(): Double = value.toDouble() / 100
  fun multi(other: TwoDecimalNumber) = TwoDecimalNumber(value.times(other.value))
  fun plus(other: TwoDecimalNumber) = TwoDecimalNumber(this.value.plus(other.value))
  fun minus(other: TwoDecimalNumber) = TwoDecimalNumber(this.value.minus(other.value))
  fun applyRate(other: TwoDecimalNumber) = TwoDecimalNumber(this.value.times(other.value).div(10000))
}
