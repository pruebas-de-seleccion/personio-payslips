package com.jorgejbarra.payslips.domain

data class MonetaryAmount(private val value: TwoDecimalNumber) {
  fun applyRate(rate: Rate): MonetaryAmount = MonetaryAmount(value.applyRate(rate.value))
  fun plus(other: MonetaryAmount) = MonetaryAmount(value.plus(other.value))
  fun minus(other: MonetaryAmount) = MonetaryAmount(value.minus(other.value))
  fun decimalValue() = value.decimalValue()
}

