package com.jorgejbarra.payslips.domain

data class Rate(val value: TwoDecimalNumber) {
  fun decimalValue() = value.decimalValue()
}
