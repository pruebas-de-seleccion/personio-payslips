package com.jorgejbarra.payslips.infrastructure.configuration

import com.jorgejbarra.payslips.infrastructure.repository.PayslipLineDeserializeException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.status
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ApiControllerAdvice {
  private val logger: Logger = LoggerFactory.getLogger(ApiControllerAdvice::class.java)

  @ExceptionHandler(PayslipLineDeserializeException::class)
  fun handleAdValidationException(exception: PayslipLineDeserializeException): ResponseEntity<String> =
      status(SERVICE_UNAVAILABLE)
          .body("We are experimenting some connectivity problems, please try it later.")
          .also {
            logger.trace("Error in persistence layer. Deserialization is not possible", exception.message)
          }
}
