package com.jorgejbarra.payslips.infrastructure.configuration

import com.jorgejbarra.payslips.infrastructure.repository.RemoteFileClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.client.ClientHttpResponse
import org.springframework.web.client.DefaultResponseErrorHandler
import org.springframework.web.client.RestTemplate
import java.io.IOException

@Configuration
class RemoteFileClientConfiguration {
  @Bean
  fun remoteFileClient(@Value("\${yellow-cars.server-url}") yellowCarsServerPath: String): RemoteFileClient {
    return RemoteFileClient(yellowCarsServerPath = yellowCarsServerPath, restTemplate = restTemplate())
  }
}

internal fun restTemplate() =
    RestTemplate().apply {
      errorHandler = RestTemplateResponseErrorHandler()
    }

internal class RestTemplateResponseErrorHandler : DefaultResponseErrorHandler() {
  @Throws(IOException::class)
  override fun hasError(response: ClientHttpResponse): Boolean {
    if (response.statusCode == NOT_FOUND) {
      return false
    }

    return super.hasError(response)
  }
}
