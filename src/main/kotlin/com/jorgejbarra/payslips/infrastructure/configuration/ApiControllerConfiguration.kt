package com.jorgejbarra.payslips.infrastructure.configuration

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties
@ComponentScan("com.jorgejbarra.payslips.infrastructure.api")
class ApiControllerConfiguration
