package com.jorgejbarra.payslips.infrastructure.configuration

import com.jorgejbarra.payslips.application.GetPayslips
import com.jorgejbarra.payslips.application.UpdateTax
import com.jorgejbarra.payslips.domain.PayslipsRepository
import com.jorgejbarra.payslips.domain.TaxRepository
import com.jorgejbarra.payslips.infrastructure.repository.InMemoryTaxRepository
import com.jorgejbarra.payslips.infrastructure.repository.PayslipDeserializer
import com.jorgejbarra.payslips.infrastructure.repository.RemoteFileClient
import com.jorgejbarra.payslips.infrastructure.repository.RemoteFilePayslipsRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OthersBeanConfig {

  @Bean
  fun getPayslips(payslipsRepository: PayslipsRepository, taxRepository: TaxRepository) = GetPayslips(
      payslipsRepository = payslipsRepository,
      taxRepository = taxRepository
  )

  @Bean
  fun payslipsRepository(
      remoteFileClient: RemoteFileClient,
      payslipDeserialize: PayslipDeserializer
  ): PayslipsRepository {
    return RemoteFilePayslipsRepository(remoteFileClient, payslipDeserialize)
  }

  @Bean
  fun payslipDeserializer() = PayslipDeserializer()

  @Bean
  fun updateTax(taxRepository: TaxRepository) = UpdateTax(taxRepository)

  @Bean
  fun taxRepository(): TaxRepository = InMemoryTaxRepository()
}
