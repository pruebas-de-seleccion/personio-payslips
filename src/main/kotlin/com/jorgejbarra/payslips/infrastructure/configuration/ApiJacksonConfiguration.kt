package com.jorgejbarra.payslips.infrastructure.configuration

import com.fasterxml.jackson.annotation.JsonInclude.Include.ALWAYS
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

@Configuration
class ApiJacksonConfiguration {
  @Bean
  fun objectMapper(): ObjectMapper = Jackson2ObjectMapperBuilder().build<ObjectMapper>()
      .disable(WRITE_DATES_AS_TIMESTAMPS)
      .setSerializationInclusion(ALWAYS)
      .registerModule(Jdk8Module())
      .registerModule(JavaTimeModule())
      .registerModule(KotlinModule())
}
