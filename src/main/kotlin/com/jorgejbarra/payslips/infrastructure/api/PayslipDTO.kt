package com.jorgejbarra.payslips.infrastructure.api

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate

data class PayslipDTO(
    @get:JsonProperty("id") val id: String,
    @get:JsonProperty("vatIdNumber") val vatIdNumber: String,
    @get:JsonProperty("date") val date: LocalDate,
    @get:JsonProperty("gross") val gross: Double,
    @get:JsonProperty("nationalInsuranceRate") val nationalInsuranceRate: Double,
    @get:JsonProperty("nationalInsuranceDeductionsAmount") val nationalInsuranceDeductionsAmount: Double,
    @get:JsonProperty("taxRate") val taxRate: Double,
    @get:JsonProperty("taxAmount") val taxAmount: Double,
    @get:JsonProperty("net") val net: Double
)
