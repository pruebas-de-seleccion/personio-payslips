package com.jorgejbarra.payslips.infrastructure.api

import com.jorgejbarra.payslips.application.GetPayslips
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.Month
import java.time.Year

@RestController
class GetPayslipsController(private val getPayslips: GetPayslips) {

  @GetMapping("/api/payslips")
  internal fun getPayslips(
      @RequestParam("year") year: Year,
      @RequestParam("month") month: Month
  ): List<PayslipDTO> {
    return getPayslips.execute(year, month)
  }
}
