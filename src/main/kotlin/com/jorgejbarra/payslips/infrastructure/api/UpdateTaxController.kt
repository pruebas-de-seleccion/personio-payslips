package com.jorgejbarra.payslips.infrastructure.api

import com.fasterxml.jackson.annotation.JsonProperty
import com.jorgejbarra.payslips.application.UpdateTax
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.Month
import java.time.Year

@RestController
class UpdateTaxController(private val updateTaxMock: UpdateTax) {

  @PutMapping("/api/tax")
  internal fun putTax(
      @RequestParam("year") year: Year,
      @RequestParam("month") month: Month,
      @RequestBody taxUpdate: TaxUpdateRequest
  ) {
    updateTaxMock.execute(year, month, taxUpdate.taxRate)
  }
}

class TaxUpdateRequest(@get:JsonProperty("taxRate") val taxRate: Double)
