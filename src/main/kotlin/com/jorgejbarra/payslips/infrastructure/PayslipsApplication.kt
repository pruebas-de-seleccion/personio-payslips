package com.jorgejbarra.payslips.infrastructure

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["com.jorgejbarra.payslips.infrastructure.configuration"])
class PayslipsApplication

fun main(args: Array<String>) {
  runApplication<PayslipsApplication>(*args)
}
