package com.jorgejbarra.payslips.infrastructure.repository

import com.jorgejbarra.payslips.domain.Payslip
import com.jorgejbarra.payslips.infrastructure.repository.LineStructure.expectedLineLength

class PayslipDeserializer {
  fun read(line: String): Payslip {
    if (line.length < expectedLineLength()) {
      throw PayslipLineDeserializeException("Line too short, it must be ${expectedLineLength()}")
    }

    if (line.length > expectedLineLength()) {
      throw PayslipLineDeserializeException("Line too long, it must be ${expectedLineLength()}")
    }

    return Payslip(
        id = Bock.Id.value(line),
        vatIdNumber = Bock.VatIdNumber.value(line),
        date = Bock.Date.value(line),
        gross = Bock.Gross.value(line),
        nationalInsuranceRate = Bock.NationalInsuranceRate.value(line),
        nationalInsuranceDeductionsAmount = Bock.NationalInsuranceDeductionsAmount.value(line),
        taxRate = Bock.TaxRate.value(line),
        taxAmount = Bock.TaxAmount.value(line),
        net = Bock.Net.value(line)
    )
  }
}

