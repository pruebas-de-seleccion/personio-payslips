package com.jorgejbarra.payslips.infrastructure.repository

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

class RemoteFileClient(
    private val yellowCarsServerPath: String,
    private val restTemplate: RestTemplate
) {
  private val logger: Logger = LoggerFactory.getLogger(RemoteFilePayslipsRepository::class.java)

  fun getPayslipsFile(fileName: String): YellowCarsServerResponse {
    val url = "$yellowCarsServerPath/$fileName"

    try {
      return restTemplate.exchange<String>(url = url, method = HttpMethod.GET)
          .apply {
            logger.trace("Response status-code $statusCode for request::${url}")
          }
          .let {
            when (it.statusCode) {
              HttpStatus.NOT_FOUND -> return YellowCarsServerResponse.NotFound
              else -> YellowCarsServerResponse.PayslipsFile(it.body!!.split("\n").map(String::trim))
            }
          }
    } catch (exception: RestClientException) {
      logger.warn("There was a problem perform request::$url", exception)
      throw exception
    }
  }
}
