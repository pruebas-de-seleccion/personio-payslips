package com.jorgejbarra.payslips.infrastructure.repository

import com.jorgejbarra.payslips.domain.PayslipsCriteria
import com.jorgejbarra.payslips.domain.Rate
import com.jorgejbarra.payslips.domain.TaxRepository
import java.util.concurrent.ConcurrentHashMap

class InMemoryTaxRepository : TaxRepository {
  private val storage = ConcurrentHashMap<PayslipsCriteria, Rate>()

  override fun update(payslipsCriteria: PayslipsCriteria, rate: Rate) {
    storage[payslipsCriteria] = rate
  }

  override fun get(payslipsCriteria: PayslipsCriteria): Rate? {
    return storage[payslipsCriteria]
  }
}
