package com.jorgejbarra.payslips.infrastructure.repository

import com.jorgejbarra.payslips.domain.MonetaryAmount
import com.jorgejbarra.payslips.domain.Rate
import com.jorgejbarra.payslips.domain.TwoDecimalNumber
import com.jorgejbarra.payslips.infrastructure.repository.BlocDeserializer.LocalDateBlockDeserializer
import com.jorgejbarra.payslips.infrastructure.repository.BlocDeserializer.MonetaryAmountBlockDeserializer
import com.jorgejbarra.payslips.infrastructure.repository.BlocDeserializer.RateBlockDeserializer
import com.jorgejbarra.payslips.infrastructure.repository.BlocDeserializer.StringBlockDeserialized
import java.time.LocalDate
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField

internal object LineStructure {
  internal val blocksInALine = listOf(
      Bock.Id,
      Bock.VatIdNumber,
      Bock.Date,
      Bock.Gross,
      Bock.NationalInsuranceRate,
      Bock.NationalInsuranceDeductionsAmount,
      Bock.TaxRate,
      Bock.TaxAmount,
      Bock.Net
  )

  fun expectedLineLength() = blocksInALine.sumBy { it.characterLength }
}

internal sealed class Bock<T>(val characterLength: Int, private val blocDeserializer: BlocDeserializer<T>) {
  object Id : Bock<String>(12, StringBlockDeserialized)
  object VatIdNumber : Bock<String>(9, StringBlockDeserialized)
  object Date : Bock<LocalDate>(8, LocalDateBlockDeserializer)
  object Gross : Bock<MonetaryAmount>(8, MonetaryAmountBlockDeserializer)
  object NationalInsuranceRate : Bock<Rate>(4, RateBlockDeserializer)
  object NationalInsuranceDeductionsAmount : Bock<MonetaryAmount>(8, MonetaryAmountBlockDeserializer)
  object TaxRate : Bock<Rate>(4, RateBlockDeserializer)
  object TaxAmount : Bock<MonetaryAmount>(8, MonetaryAmountBlockDeserializer)
  object Net : Bock<MonetaryAmount>(8, MonetaryAmountBlockDeserializer)

  private fun initialIndex() = LineStructure.blocksInALine.takeWhile { this != it }.sumBy { it.characterLength }
  fun value(line: String): T {
    return this.blocDeserializer.read(line.drop(this.initialIndex()).take(this.characterLength))
  }
}

private sealed class BlocDeserializer<T> {
  abstract fun read(line: String): T

  object LocalDateBlockDeserializer : BlocDeserializer<LocalDate>() {
    override fun read(line: String): LocalDate {
      return LocalDate.parse(line, DateTimeFormatterBuilder()
          .appendValue(ChronoField.YEAR, 4)
          .appendValue(ChronoField.MONTH_OF_YEAR, 2)
          .appendValue(ChronoField.DAY_OF_MONTH, 2)
          .toFormatter())
    }
  }

  object RateBlockDeserializer : BlocDeserializer<Rate>() {
    override fun read(line: String): Rate {
      return Rate(TwoDecimalNumber(line.toLong()))
    }
  }

  object MonetaryAmountBlockDeserializer : BlocDeserializer<MonetaryAmount>() {
    override fun read(line: String): MonetaryAmount {
      return MonetaryAmount(TwoDecimalNumber(line.toLong()))
    }
  }

  object StringBlockDeserialized : BlocDeserializer<String>() {
    override fun read(line: String): String {
      return line
    }
  }
}
