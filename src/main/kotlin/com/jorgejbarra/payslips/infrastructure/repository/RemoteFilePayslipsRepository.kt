package com.jorgejbarra.payslips.infrastructure.repository

import com.jorgejbarra.payslips.domain.Payslip
import com.jorgejbarra.payslips.domain.PayslipsCriteria
import com.jorgejbarra.payslips.domain.PayslipsRepository
import com.jorgejbarra.payslips.infrastructure.repository.YellowCarsServerResponse.NotFound
import com.jorgejbarra.payslips.infrastructure.repository.YellowCarsServerResponse.PayslipsFile

class RemoteFilePayslipsRepository(
    private val remoteFileClient: RemoteFileClient,
    private val payslipDeserialize: PayslipDeserializer
) : PayslipsRepository {

  override fun find(criteria: PayslipsCriteria): List<Payslip> {
    val fileName = "payslips." + "%04d".format(criteria.year) + "%02d".format(criteria.month) + ".txt"
    return when (val payslipsFile = remoteFileClient.getPayslipsFile(fileName)) {
      NotFound -> emptyList()
      is PayslipsFile -> payslipsFile.lines.map(payslipDeserialize::read)
    }
  }
}
