package com.jorgejbarra.payslips.infrastructure.repository

sealed class YellowCarsServerResponse {
  object NotFound : YellowCarsServerResponse()
  data class PayslipsFile(val lines: List<String>) : YellowCarsServerResponse()
}
