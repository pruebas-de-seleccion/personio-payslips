package com.jorgejbarra.payslips.infrastructure.repository

class PayslipLineDeserializeException(message: String) : RuntimeException(message)
