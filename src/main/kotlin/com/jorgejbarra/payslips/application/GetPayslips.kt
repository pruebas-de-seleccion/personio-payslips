package com.jorgejbarra.payslips.application

import com.jorgejbarra.payslips.domain.Payslip
import com.jorgejbarra.payslips.domain.PayslipsCriteria
import com.jorgejbarra.payslips.domain.PayslipsRepository
import com.jorgejbarra.payslips.domain.Rate
import com.jorgejbarra.payslips.domain.TaxRepository
import com.jorgejbarra.payslips.infrastructure.api.PayslipDTO
import java.time.Month
import java.time.Year

class GetPayslips(private val payslipsRepository: PayslipsRepository, private val taxRepository: TaxRepository) {
  fun execute(year: Year, month: Month): List<PayslipDTO> {
    val criteria = PayslipsCriteria(year.value, month.value)
    val tax = taxRepository.get(criteria)

    return payslipsRepository.find(criteria).map { it.applyTax(tax) }.map(this::toDto)
  }

  private fun toDto(source: Payslip) = PayslipDTO(
      id = source.id,
      vatIdNumber = source.vatIdNumber,
      date = source.date,
      gross = source.gross.decimalValue(),
      nationalInsuranceRate = source.nationalInsuranceRate.decimalValue(),
      nationalInsuranceDeductionsAmount = source.nationalInsuranceDeductionsAmount.decimalValue(),
      taxRate = source.taxRate.decimalValue(),
      taxAmount = source.taxAmount.decimalValue(),
      net = source.net.decimalValue()
  )

  private fun Payslip.applyTax(tax: Rate?) = tax?.let { this.updateTaxRate(it) } ?: this
}
