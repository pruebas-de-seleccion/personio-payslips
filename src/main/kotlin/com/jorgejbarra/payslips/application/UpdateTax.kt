package com.jorgejbarra.payslips.application

import com.jorgejbarra.payslips.domain.PayslipsCriteria
import com.jorgejbarra.payslips.domain.Rate
import com.jorgejbarra.payslips.domain.TaxRepository
import com.jorgejbarra.payslips.domain.TwoDecimalNumber
import java.time.Month
import java.time.Year

class UpdateTax(private val taxRepository: TaxRepository) {
  fun execute(year: Year, month: Month, taxRate: Double) {
    taxRepository.update(PayslipsCriteria(year.value, month.value), taxRate.toRate())
  }

  private fun Double.toRate(): Rate = Rate(TwoDecimalNumber(this.times(100).toLong()))
}

