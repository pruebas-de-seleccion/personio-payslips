package com.jorgejbarra.payslips

import com.jorgejbarra.payslips.infrastructure.api.PayslipDTO
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.time.Month

@Component
class ApiClient(private val restTemplate: TestRestTemplate) {
  fun getPayslip(year: Int, month: Month): ResponseEntity<Array<PayslipDTO>> {
    return restTemplate.getForEntity("/api/payslips?year=$year&month=$month", Array<PayslipDTO>::class.java)
  }

  fun modifyTax(year: Int, month: Month, taxRate: Double) {
    restTemplate.put(
        "/api/tax?year=$year&month=$month",
        HttpEntity("""{"taxRate":$taxRate}""", basicHeaders()),
        Void::class.java
    )
  }

  private fun basicHeaders() = HttpHeaders().apply {
    contentType = MediaType.APPLICATION_JSON
  }
}
