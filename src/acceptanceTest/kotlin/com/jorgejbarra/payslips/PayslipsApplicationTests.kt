package com.jorgejbarra.payslips

import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.notFound
import com.github.tomakehurst.wiremock.client.WireMock.ok
import com.github.tomakehurst.wiremock.client.WireMock.stubFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.jorgejbarra.payslips.infrastructure.PayslipsApplication
import com.jorgejbarra.payslips.infrastructure.api.PayslipDTO
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.http.HttpStatus.OK
import org.springframework.test.context.ActiveProfiles
import java.time.LocalDate
import java.time.Month.AUGUST
import java.time.Month.DECEMBER

@AutoConfigureWireMock
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = [PayslipsApplication::class, ApiClient::class])
class PayslipsApplicationTests {

  @Autowired
  private lateinit var apiClient: ApiClient

  @Test
  internal fun `should retrieve payslips given year and month`() {
    //given
    val year = 2018
    val month = DECEMBER
    stubFor(get(urlPathEqualTo("/payslips.201812.txt"))
        .willReturn(ok("00000000042760016472L201812310026130005000001306513000003396900214266")))

    //when
    val response = apiClient.getPayslip(year, month)

    //then
    assertThat(response.statusCode).isEqualTo(OK)
    assertThat(response.body).usingRecursiveFieldByFieldElementComparator().containsExactly(
        PayslipDTO(
            id = "000000000427",
            vatIdNumber = "60016472L",
            date = LocalDate.of(2018, 12, 31),
            gross = 2613.0,
            nationalInsuranceRate = 5.0,
            nationalInsuranceDeductionsAmount = 130.65,
            taxRate = 13.0,
            taxAmount = 339.69,
            net = 2142.66
        )
    )
  }

  @Test
  internal fun `should retrieve empty payslips list when we dont have data`() {
    //given
    val year = 2018
    val month = DECEMBER
    stubFor(get(urlPathEqualTo("/payslips.201812.txt"))
        .willReturn(notFound()))

    //when
    val response = apiClient.getPayslip(year, month)

    //then
    assertThat(response.statusCode).isEqualTo(OK)
    assertThat(response.body).isEmpty()
  }

  @Test
  internal fun `should retrieve modify payslips when tax was changed`() {
    //given
    val year = 2008
    val month = AUGUST
    stubFor(get(urlPathEqualTo("/payslips.200808.txt"))
        .willReturn(ok("00000000042760016472L201812310026130005000001306513000003396900214266")))
    val updatedTaxRate = 10.00

    //when
    apiClient.modifyTax(year, month, updatedTaxRate)
    val response = apiClient.getPayslip(year, month)

    //then
    assertThat(response.statusCode).isEqualTo(OK)
    assertThat(response.body).usingRecursiveFieldByFieldElementComparator().containsExactly(
        PayslipDTO(
            id = "000000000427",
            vatIdNumber = "60016472L",
            date = LocalDate.of(2018, 12, 31),
            gross = 2613.0,
            nationalInsuranceRate = 5.0,
            nationalInsuranceDeductionsAmount = 130.65,
            taxRate = updatedTaxRate,
            taxAmount = 261.30,
            net = 2221.05
        )
    )
  }
}
