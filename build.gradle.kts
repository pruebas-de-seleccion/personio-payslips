import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("org.springframework.boot") version "2.2.5.RELEASE"
  id("io.spring.dependency-management") version "1.0.9.RELEASE"
  id("idea")
  kotlin("jvm") version "1.3.61"
  kotlin("plugin.spring") version "1.3.61"
}

group = "com.jorgejbarra"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
  mavenCentral()
}

apply(from = "gradle/integration-test.gradle.kts")
apply(from = "gradle/acceptance-test.gradle.kts")

dependencies {
  implementation("org.springframework.boot:spring-boot-starter-web")
  implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
  implementation("org.jetbrains.kotlin:kotlin-reflect")
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

  testImplementation("org.springframework.boot:spring-boot-starter-test") {
    exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
  }
  testImplementation("com.nhaarman:mockito-kotlin:1.6.0")
  testImplementation("org.mockito:mockito-inline:2.13.0")
  testImplementation("io.rest-assured:spring-mock-mvc:3.1.1")
  testImplementation("org.springframework.cloud:spring-cloud-contract-wiremock:2.2.2.RELEASE")
}

tasks.withType<Test> {
  useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "1.8"
  }
}

idea.module {
  testSourceDirs = testSourceDirs.union(sourceSets["integrationTest"].allSource.srcDirs)
  testSourceDirs = testSourceDirs.union(sourceSets["acceptanceTest"].allSource.srcDirs)
}
