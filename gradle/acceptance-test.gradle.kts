val sourceSets = the<SourceSetContainer>()

sourceSets {
  create("acceptanceTest") {
    java.srcDir(file("src/acceptanceTest/java"))
    resources.srcDir(file("src/acceptanceTest/resources"))
    compileClasspath += sourceSets["main"].output + sourceSets["test"].output
    runtimeClasspath += sourceSets["main"].output + sourceSets["test"].output
  }
}

configurations {
  getAt("acceptanceTestCompile").extendsFrom(getAt("testCompile"))
  getAt("acceptanceTestImplementation").extendsFrom(getAt("testImplementation"))
  getAt("acceptanceTestRuntime").extendsFrom(getAt("testRuntime"))
}

tasks.register<Test>("acceptanceTest") {
  group = LifecycleBasePlugin.VERIFICATION_GROUP
  description = "Runs the acceptance tests."
  testClassesDirs = sourceSets["acceptanceTest"].output.classesDirs
  classpath = sourceSets["acceptanceTest"].runtimeClasspath
  mustRunAfter(tasks["test"])
}

tasks.named("check") {
  dependsOn("acceptanceTest")
}



