val sourceSets = the<SourceSetContainer>()

sourceSets {
  create("integrationTest") {
    java.srcDir(file("src/integrationTest/java"))
    resources.srcDir(file("src/integrationTest/resources"))
    compileClasspath += sourceSets["main"].output + sourceSets["test"].output
    runtimeClasspath += sourceSets["main"].output + sourceSets["test"].output
  }
}

configurations {
  getAt("integrationTestCompile").extendsFrom(getAt("testCompile"))
  getAt("integrationTestImplementation").extendsFrom(getAt("testImplementation"))
  getAt("integrationTestRuntime").extendsFrom(getAt("testRuntime"))
}

tasks.register<Test>("integrationTest") {
  group = LifecycleBasePlugin.VERIFICATION_GROUP
  description = "Runs the integration tests."
  testClassesDirs = sourceSets["integrationTest"].output.classesDirs
  classpath = sourceSets["integrationTest"].runtimeClasspath
  mustRunAfter(tasks["test"])
}

tasks.named("check") {
  dependsOn("integrationTest")
}



